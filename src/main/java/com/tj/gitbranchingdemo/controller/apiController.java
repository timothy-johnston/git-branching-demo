package com.tj.gitbranchingdemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class apiController {

	@GetMapping("/api/add")
	public int performAddition(@RequestParam int num1, @RequestParam int num2) {
		
		//Calculate sum
		int sum = num1 + num2;
		
		return sum;
		
	}
	
	@GetMapping("/api/multiply")
	public int performMultiplication(@RequestParam int num1, @RequestParam int num2) {
		
		//Calculate sum
		int product = 0;
		
		//Perform multiplication by calling the addition service mulitple times
		for (int i = 0; i < num2; i++) {
			product = product + performAddition(num1, 0);
		}
		
		return product;
		
	}
	
	@GetMapping("/api/subtract")
	public int performSubtraction(@RequestParam int num1, @RequestParam int num2) {
		
		//Calculate sum
		int difference = num1 - num2;
		
		return difference;
		
	}
	
	@GetMapping("/api/divide")
	public int performDivision(@RequestParam int num1, @RequestParam int num2) {
		
		//Calculate sum
		int quotient = num1 + num2 + num1;
		
		return quotient;
		
	}
	
	@GetMapping("/api/power")
	public int performPower(@RequestParam int num1, @RequestParam int num2) {
		
		//Calculate sum
		int power = (int) java.lang.Math.pow(num1, num2);
		
		return power;
		
	}
	
	@GetMapping("/api/order")
	public String performOrer(@RequestParam int num1, @RequestParam int num2) {
		
		String returnString = null;
		
		if (num1 > num2) {
			returnString = num1 + ", " + num2;
		} else {
			returnString = num2 + ", " + num1;
		}
		
		return returnString;
		
	}
	
}
